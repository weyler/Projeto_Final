class CreateJogos < ActiveRecord::Migration
  def change
    create_table :jogos do |t|
      t.text :Nome, :limit => 10
      t.text :Description, :limit => 30

      t.timestamps null: false
    end
  end
end
