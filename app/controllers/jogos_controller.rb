class JogosController < ApplicationController
  before_action :set_jogo, only: [:show, :edit, :update, :destroy]

  # GET /jogos
  # GET /jogos.json
  def index
    @jogos = Jogo.all
  end
  def meus_jogos
    @jogos = Jogo.all
  end

  # GET /jogos/1
  # GET /jogos/1.json
  def show
  end

  # GET /jogos/new
  def new
    @jogo = Jogo.new
  end

  # GET /jogos/1/edit
  def edit
    authorize! :update, @jogo
  end

  # POST /jogos
  # POST /jogos.json
  def create
    @jogo = Jogo.new(jogo_params)
    @jogo.user = current_user
    respond_to do |format|
      if @jogo.save
        format.html { redirect_to @jogo, notice: 'O jogo foi publicado com sucesso' }
        format.json { render :show, status: :created, location: @jogo }
      else
        format.html { render :new }
        format.json { render json: @jogo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jogos/1
  # PATCH/PUT /jogos/1.json
  def update
    authorize! :update, @jogo
    respond_to do |format|
      if @jogo.update(jogo_params)
        format.html { redirect_to @jogo, notice: 'O jogo foi atualizado com sucesso' }
        format.json { render :show, status: :ok, location: @jogo }
      else
        format.html { render :edit }
        format.json { render json: @jogo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jogos/1
  # DELETE /jogos/1.json
  def destroy
    authorize! :destroy, @jogo
    @jogo.destroy
    respond_to do |format|
      format.html { redirect_to jogos_url, notice: 'A publicação foi apagada com sucesso' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jogo
      @jogo = Jogo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jogo_params
      params.require(:jogo).permit(:Nome, :Description, :user_id)
    end
end
